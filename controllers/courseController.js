const Course = require("../models/Course");
const auth = require('../auth');

//Route for adding course
module.exports.addCourse = (reqBody) => {

		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
		
			if (error){
				return false
			} else {
				return true
			}
		})

}
// Promise.resolve()



// retrieve all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}



// retrieve all the active courses
module.exports.getAllActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}



// retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


// update a course
module.exports.updateCourse = (reqParams, reqBody) => {

	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
/*
Syntax:
findByIdAndUpdate(document ID, updatesToBeApplied)
*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		if (error){
			return false
		} else {
			return true
		}
	})
}



// [ACTIVITY]
// Archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {

	let archiveCourse = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}
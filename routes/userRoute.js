const express = require('express');

const router = express.Router();

const userController = require('../controllers/userController');

const auth = require("../auth");


// Route for checking if the user's email already exists in our database
router.post('/checkEmail', (req, res) => {

	userController.checkEmailExists(req.body).then((resultFromController) => {
		res.send(resultFromController);
	}) 
})


// Route for User Registration
router.post('/register', (req, res) => {

	userController.registerUser(req.body).then((resultFromController) => {
		res.send(resultFromController)
	})
})



// Route for User Authentication
router.post('/login', (req, res) => {

	userController.loginUser(req.body).then(resultFromController => {
		res.send(resultFromController)
	}) 
})



// Route for Retrieving User Details
// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.post('/details', auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	/*{
  id: '63dc65b0ac5ee51c7bf67751',
  email: 'john@mail.com',
  isAdmin: false,
  iat: 1675405018
	}*/

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({_id: userData.id}).then(resultFromController => {
		res.send(resultFromController)
	})
})



//Route to enroll user to a course
/*router.post('/enroll', (req, res) => {
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId,
	}
	userController.enroll(data).then(resultFromController => {
		res.send(resultFromController
			)
	})
})*/

router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => {
		res.send(resultFromController
			)
	})
})





module.exports = router;
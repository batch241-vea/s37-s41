const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {

	/* {
  _id: new ObjectId("63dc65b0ac5ee51c7bf67751"),
  firstName: 'John',
  lastName: 'Smith',
  email: 'john@mail.com',
  password: '$2b$10$uYall4/lSKkSfZBf0/aMbeA6Dku5O4RtxKPGIOuozuYQ96rjqV8oO',
  isAdmin: false,
  mobileNo: '09123456789',
  enrollments: [],
  _v: 0
  */

	//payload
	// When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a token
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	//jwt.sign(data/payload, secretkey, options);
	return jwt.sign(data, secret, {});

}


//Token Verification

module.exports.verify = (req, res, next) => {

	// The token is retrieved from the request header
	// This can be provided in postman under
	// Authorization > Bearer Token
	let token = req.headers.authorization;

	if(typeof token !== "undefined") {

		console.log(token);

		// The "slice" method takes only the token from the information sent via the request header
		// The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
		// This removes the "Bearer " prefix and obtains only the token for verification
		token = token.slice(7, token.length);

		// Validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"});
			} else {
				// Allows the application to proceed with the next middleware function/callback function in the route
				// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
				next();
			}
		})
	} else {
		return res.send({auth: "failed"});
	}

}


//Token Decryption
module.exports.decode = (token) => {

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error){
				return null
			} else {
				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {
		return null
	}
}